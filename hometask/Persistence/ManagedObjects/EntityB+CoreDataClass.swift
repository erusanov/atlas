//
//  EntityB+CoreDataClass.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//
//

import Foundation
import CoreData

@objc(EntityB)
public class EntityB: EntityA {
}
