//
//  EntityA+CoreDataProperties.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//
//

import Foundation
import CoreData


extension EntityA {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<EntityA> {
        return NSFetchRequest<EntityA>(entityName: "EntityA")
    }

    @NSManaged public var title: String?
    @NSManaged public var descr: String?
    @NSManaged public var value: Int32
    @NSManaged public var image: Data?

}

extension EntityA : Identifiable {

}
