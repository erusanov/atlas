//
//  EntityB+CoreDataProperties.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//
//

import Foundation
import CoreData


extension EntityB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<EntityB> {
        return NSFetchRequest<EntityB>(entityName: "EntityB")
    }

    @NSManaged public var labels: NSArray?

}
