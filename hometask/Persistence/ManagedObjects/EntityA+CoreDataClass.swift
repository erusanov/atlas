//
//  EntityA+CoreDataClass.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//
//

import UIKit
import CoreData

@objc(EntityA)
public class EntityA: NSManagedObject {
}
