//
//  CoreData~Setup.swift
//  LocalPersistance
//
//  Created by AP Yauhen Rusanau on 6/19/20.
//

import Foundation
import CoreData
import Combine

class CoreDataStack {
    enum Mode {
        case `default`
        case memory
    }

    static let modelName = "Model"

    private let container: NSPersistentContainer = {
        let bundle = Bundle(for: CoreDataStack.self)

        guard
            let modelUrl = bundle.url(forResource: modelName, withExtension: "momd"),
            let model = NSManagedObjectModel(contentsOf: modelUrl)
        else { fatalError("Can't find model file") }
    
        return NSPersistentContainer(name: modelName, managedObjectModel: model)
    }()

    init(mode: Mode = .default) {
        if mode == .memory {
            let description = NSPersistentStoreDescription()
            description.type = NSInMemoryStoreType
            description.shouldAddStoreAsynchronously = false // Make it simpler in test env
            container.persistentStoreDescriptions = [description]
        }

        container.loadPersistentStores { (_, error) in
            if let error = error { fatalError("Can't load persistence: \(error.localizedDescription)") }
        }
    }
    
    var mainContext: NSManagedObjectContext { container.viewContext }
    var scratchPad: NSManagedObjectContext {
        let child = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        child.parent = mainContext
        return child
    }
    
    func store(context: NSManagedObjectContext? = nil) {
        let contextToSave = context ?? mainContext
        contextToSave.store()
    }
}

extension NSManagedObjectContext {
    func store() {
        guard hasChanges else { return }
        
        do {
          try save()
        } catch let error as NSError {
          print("Fail to save context \(error), \(error.userInfo)")
        }
        
        if let parent = parent {
            parent.store()
            parent.reset()
        }
    }
}
