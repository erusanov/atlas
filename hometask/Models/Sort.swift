//
//  Sort.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//

import Foundation

enum Sort: String, CaseIterable, Identifiable {
    var id: String { rawValue }
    
    case title
    case description
    case value
    case type
}
