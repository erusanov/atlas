//
//  MainScreenCoordinator.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//

import SwiftUI

class MainScreenCoordinator: Coordinator {
    let stack = CoreDataStack()
    
    func start() -> some View {
        NavigationView {
            MainScreen(viewModel: .init(coordinator: self, context: stack.mainContext))
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

extension MainScreenCoordinator {
    func presentEditA(_ binding: Binding<Bool>, completion: @escaping () -> Void) -> some View {
        ModalLink(destination: NavigationView { EditEntityCoordinator(context: self.stack.scratchPad,
                                                                      type: EntityA.self,
                                                                      didFinish: { binding.wrappedValue = false; completion() }).start() },
                  isActive: binding)
    }
    
    func presentEditB(_ binding: Binding<Bool>, completion: @escaping () -> Void) -> some View {
        ModalLink(destination: NavigationView { EditEntityCoordinator(context: self.stack.scratchPad,
                                                                      type: EntityB.self,
                                                                      didFinish: { binding.wrappedValue = false; completion() }).start() }, isActive: binding)
    }
    
    func presentExistingEdit(_ entity: EntityA, binding: Binding<Bool>, completion: @escaping () -> Void) -> some View {
        NavigationLink(destination: LazyView( EditEntityCoordinator(context: self.stack.scratchPad, entity: entity, isNew: false,
                                                          didFinish: { binding.wrappedValue = false; completion() }).start() ),
                       isActive: binding) { EmptyView() }
    }
}
