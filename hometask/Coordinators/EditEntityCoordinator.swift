//
//  AddEntityCoordinator.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/18/20.
//

import Foundation
import SwiftUI
import CoreData

class EditEntityCoordinator: Coordinator {
    private let context: NSManagedObjectContext
    private let entity: EntityA
    private let didFinish: () -> Void
    
    let isNew: Bool
    
    convenience init<T: EntityA>(context: NSManagedObjectContext, type: T.Type, didFinish: @escaping ()->Void) {
        let entity = type.init(context: context)
        
        entity.title = .random(length: 50)
        entity.descr = .random(length: 300)
        entity.value = .random(in: -100...100)
        
        if let entityB = entity as? EntityB {
            entityB.labels = [] as NSArray
        }
        
        self.init(context: context, entity: entity, isNew: true, didFinish: didFinish)
    }
    
    init(context: NSManagedObjectContext, entity: EntityA, isNew: Bool, didFinish: @escaping ()->Void) {
        self.context = context
        self.didFinish = didFinish
        
        self.isNew = isNew
        
        if !isNew {
            guard let copied = context.object(with: entity.objectID) as? EntityA else { fatalError("Can't move entity to child context") }
            self.entity = copied
        }
        else {
            self.entity = entity
        }        
    }
    
    func start() -> some View {
        EntityEdit(viewModel: .init(entity: self.entity, isNew: isNew, output: self))
    }
}

extension EditEntityCoordinator {
    func didCancel() {
        didFinish()
    }
    
    func saveAndFinish() {
        context.store()
        didFinish()
    }
    
    func presentPicker(_ binding: Binding<Bool>, didSelect: @escaping (UIImage) -> Void) -> some View {
        ModalLink(destination: PhotoPicker(isPresented: binding, didSelectPicture: didSelect),
                  isActive: binding)
        
    }
}
