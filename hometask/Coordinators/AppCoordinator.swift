//
//  AppCoordinator.swift
//  Nutrition
//
//  Created by AP Yauhen Rusanau on 8/27/20.
//

import UIKit
import SwiftUI

class AppCoordinator: Coordinator {
    private let window: UIWindow
    private let mainScreen = MainScreenCoordinator()

    init(window: UIWindow) {
        self.window = window
    }

    @discardableResult
    func start() -> some View {
        window.rootViewController = UIHostingController(rootView: coordinate(to: mainScreen))
        window.makeKeyAndVisible()
        return EmptyView()
    }
}
