//
//  String+random.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/18/20.
//

import Foundation

extension String {
    static func random(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
}
