//
//  Fatal.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//

import Foundation

func fatalError<T>(_ message: @autoclosure () -> String = #function, file: StaticString = #file, line: UInt = #line) -> T {
    Swift.fatalError(message(), file: file, line: line)
}
