//
//  LazyNavigationView.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/18/20.
//

import Foundation
import SwiftUI

struct LazyView<Content: View>: View {
    let build: () -> Content
    init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }
    var body: Content {
        build()
    }
}
