//
//  ModalLink.swift
//  CommonUI
//
//  Created by AP Yauhen Rusanau on 8/27/20.
//

import SwiftUI

public struct ModalLink<T: View>: View {
    public init(destination: @escaping @autoclosure () -> T, isActive: Binding<Bool>) {
        self.isActive = isActive
        self.destination = destination
    }

    let isActive: Binding<Bool>
    let destination: () -> T

    public var body: some View {
        EmptyView()
            .sheet(isPresented: isActive, content: {
                self.destination()
            })
    }
}
