//
//  NavigationButton.swift
//  CommonUI
//
//  Created by AP Yauhen Rusanau on 8/27/20.
//

import SwiftUI

public struct NavigationButton<Content: View, Navigation: View>: View {
    public init(contentView: Content, navigationView: @escaping (Binding<Bool>) -> Navigation) {
        self.contentView = contentView
        self.navigationView = navigationView
    }

    @State private var isPresented = false

    let contentView: Content
    let navigationView: (Binding<Bool>) -> Navigation

    public var body: some View {
        Button(action: {
            self.isPresented = true
        }) { contentView.background(navigationView($isPresented)) }
    }
}
