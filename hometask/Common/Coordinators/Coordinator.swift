//
//  Coordinator.swift
//  CommonUI
//
//  Created by AP Yauhen Rusanau on 8/27/20.
//

import SwiftUI

public protocol Coordinator {
    associatedtype Content: View
    func start() -> Content
}

public extension Coordinator {
    func coordinate<T: Coordinator>(to coordinator: T) -> some View {
        return coordinator.start()
    }
}
