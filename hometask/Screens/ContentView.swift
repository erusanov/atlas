//
//  ContentView.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
