//
//  EntityAEdit~ViewModel.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//

import UIKit
import SwiftUI
import CoreData

protocol EntityAEditOutput {
    func didCancel()
    func saveAndFinish()
}

extension EntityEdit {
    class ViewModel: ObservableObject {
        public init(entity: EntityA?, isNew: Bool, output: EditEntityCoordinator?) {
            self.isNew = isNew
            self.entity = entity
            self.output = output
            self.hasLabels = entity is EntityB
            
            title = entity?.title ?? .random(length: 50)
            description = entity?.descr ?? .random(length: 300)
            value = Int(entity?.value ?? .random(in: -100...100))
            image = entity?.image.flatMap { UIImage(data: $0) }
            
            labels = ((entity as? EntityB)?.labels as? [String])?.joined(separator: " ") ?? ""
        }
        
        var isNew: Bool
        var hasLabels: Bool
        
        @Published var title: String {
            didSet { entity?.title = title }
        }
        @Published var description: String {
            didSet { entity?.descr = description }
        }
        @Published var value: Int {
            didSet { entity?.value = Int32(value) }
        }
        @Published var image: UIImage? {
            didSet { entity?.image = image?.pngData() }
        }
        @Published var labels: String {
            didSet { (entity as? EntityB)?.labels = (labels.components(separatedBy: " ")) as NSArray }
        }
        
        private var entity: EntityA?
        private let output: EditEntityCoordinator?
        
        func updatePicture(_ binding: Binding<Bool>) -> some View {
            output?.presentPicker(binding, didSelect: { [weak self] in self?.image = $0 })
        }
        
        func cancel() {
            output?.didCancel()
        }
        
        func save() {
            output?.saveAndFinish()
        }
    }
}
