//
//  EntityAEdit.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//

import SwiftUI

extension Image {
    static var placeholder: some View {
        Image(systemName: "questionmark.circle")
            .resizable()
            .foregroundColor(.secondary)
    }
}

struct EntityEdit: View {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        Form {
            Section {
                HStack{
                    Text("Title")
                    TextField("Enter title", text: $viewModel.title)
                        .foregroundColor(.primary)
                }
                HStack {
                    Text("Description")
                    TextEditor(text: $viewModel.description)
                }
                HStack {
                    Stepper(value: $viewModel.value, step: 1, onEditingChanged: {_ in }) {
                        HStack {
                            Text("Value")
                            Spacer()
                            Text("\(viewModel.value)")
                                .foregroundColor(.primary)
                            Spacer()
                        }
                    }
                }
            }
            
            Section {
                NavigationButton(contentView:
                    HStack {
                        Text("Choose picture")
                        Spacer()
                        ZStack {
                            if let image = viewModel.image {
                                Image(uiImage: image)
                                    .resizable()
                                    .scaledToFit()
                            }
                            else {
                                Image.placeholder
                            }
                        }
                        .frame(width: 75, height: 75, alignment: .center)
                }, navigationView: { viewModel.updatePicture($0) })
            }
            
            if viewModel.hasLabels {
                Section(header: Text("Labels")) {
                    TextEditor(text: $viewModel.labels)
                }
            }
        }
        .navigationBarTitle(viewModel.isNew ? "Create" : "Edit", displayMode: .inline)
        .navigationBarItems(leading: Button("Cancel", action: { viewModel.cancel() }),
                            trailing: Button("Save", action: { viewModel.save() }))
        .navigationBarBackButtonHidden(true)
        .allowAutoDismiss( { !viewModel.isNew } )
    }
}

struct EntityAEdit_Previews: PreviewProvider {
    static var previews: some View {
        EntityEdit(viewModel: .init(entity: nil, isNew: true, output: nil))
    }
}
