//
//  MainScreen~ViewModel.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//

import Foundation
import SwiftUI
import Combine
import CoreData

extension MainScreen {
    class ViewModel: ObservableObject {
        @Published var items: [EntityA] = []
        
        private let coordinator: MainScreenCoordinator?
        private let context: NSManagedObjectContext
        
        init(coordinator: MainScreenCoordinator?, context: NSManagedObjectContext) {
            self.coordinator = coordinator
            self.context = context
            
            updateFetchRequest()
        }
        
        @Published var sort: Sort = .title {
            didSet {
                updateFetchRequest()
            }
        }
        
        func addEntityA(_ binding: Binding<Bool>) -> some View {
            coordinator?.presentEditA(binding, completion: { [weak self] in
                self?.updateFetchRequest()
            })
        }
        
        func addEntityB(_ binding: Binding<Bool>) -> some View {
            coordinator?.presentEditB(binding, completion: { [weak self] in
                self?.updateFetchRequest()
            })
        }
        
        func didSelectEntity(_ entity: EntityA, binding: Binding<Bool>) -> some View {
            coordinator?.presentExistingEdit(entity, binding: binding, completion: { [weak self] in
                self?.updateFetchRequest()
            })
        }
        
        private func updateFetchRequest() {
            let fetchRequest: NSFetchRequest = EntityA.fetchRequest()
            fetchRequest.sortDescriptors = [sort.descriptor]
            
            items = sort.postprocessResult((try? context.fetch(fetchRequest)) ?? [])
        }
    }
}

fileprivate extension Sort {
    var descriptor: NSSortDescriptor {
        switch self {
        case .title:
            return .init(keyPath: \EntityA.title, ascending: true)
        case .description:
            return .init(keyPath: \EntityA.descr, ascending: true)
        case .value:
            return .init(keyPath: \EntityA.value, ascending: true)
        case .type:
            return Self.title.descriptor
        }
    }
    
    func postprocessResult(_ result: [EntityA]) -> [EntityA] {
        switch self {
        case .type:
            return result.sorted(by: { !($0 is EntityB) && ($1 is EntityB) })
        default:
            return result
        }
    }
}
