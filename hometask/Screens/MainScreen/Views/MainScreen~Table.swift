//
//  MainScreen~Table.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/18/20.
//

import SwiftUI

extension MainScreen {
    struct Table: View {
        @ObservedObject var viewModel: ViewModel
        
        var body: some View {
            List(viewModel.items, id: \.objectID) { row in
                NavigationButton(contentView: Cell(entity: row),
                                 navigationView: { viewModel.didSelectEntity(row, binding: $0) })                
            }
        }
    }

}
