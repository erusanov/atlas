//
//  MainScreen~Cell.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/18/20.
//

import SwiftUI

extension MainScreen {
    struct Cell: View {
        let entity: EntityA
        
        var body: some View {
            HStack {
                ZStack {
                    if let image = entity.image.flatMap({ UIImage(data: $0) }) {
                        Image(uiImage: image)
                            .resizable()
                            .scaledToFit()
                    }
                    else {
                        Image.placeholder
                    }
                    
                    if let labels = ((entity as? EntityB)?.labels as? [String])?.joined(separator: " ") {
                        Text(labels)
                            .font(.system(size: 500))
                            .minimumScaleFactor(0.01)
                            .opacity(0.5)
                    }
                }
                .frame(width: 75, height: 75, alignment: .center)
                .padding([.leading, .trailing])
                
                VStack {
                    HStack {
                        Text(entity.title ?? "test title")
                            .font(.title)
                            .bold()
                        
                        Spacer()
                        
                        Text("\(entity.value)")
                            .font(.title2)
                            .italic()
                            .lineLimit(1)
                            .padding(.trailing)
                    }
                    Spacer()
                    Text(entity.descr ?? "test description test descripion description test descripion description test descripion")
                        .padding(.trailing)
                }
            }
            .padding([.top, .bottom])
        }
    }

}

struct MainScreen_Cell_Previews: PreviewProvider {
    static var previews: some View {
        let stack = CoreDataStack(mode: .memory)
        let entity = EntityA(context: stack.mainContext)
        
        entity.title = "test title"
        entity.descr = "test description test description test description test description test description"
        entity.value = 1234
        
        return MainScreen.Cell(entity: entity)
    }
}
