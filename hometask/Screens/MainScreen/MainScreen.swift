//
//  MainScreen.swift
//  hometask
//
//  Created by AP Yauhen Rusanau on 9/17/20.
//

import SwiftUI

struct MainScreen: View {
    var viewModel: ViewModel
    @State private var showingSorting = false
    
    var body: some View {
        Table(viewModel: viewModel)
            .navigationBarItems(leading:
                                    Button("Sort - \(viewModel.sort.rawValue)", action: { showingSorting = true })
                                    .popSheet(isPresented: $showingSorting) {
                                        PopSheet(title: Text("Choose sort"),
                                                 message: nil,
                                                 buttons: Sort.allCases.map { mode in PopSheet.Button(kind: .default,
                                                                                                      label: Text(mode.rawValue),
                                                                                                      action: { viewModel.sort = mode }) })
                                    }, trailing:
                                        HStack {
                                            NavigationButton(contentView: Text("Add A")) {
                                                viewModel.addEntityA($0)
                                            }
                                            NavigationButton(contentView: Text("Add B")) {
                                                viewModel.addEntityB($0)
                                            }
                                        })
            .navigationBarHidden(false)
            .navigationBarTitle("Catalog", displayMode: .inline)
    }
}

struct MainScreen_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            MainScreen(viewModel: .init(coordinator: nil,
                                        context: CoreDataStack(mode: .memory).mainContext))
        }
    }
}
